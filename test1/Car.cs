﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace test1
{
    public class Car
    {
        [XmlElement]
        public string StockNumber { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
    }

    [XmlRoot("Cars")]
    public class CarCollection
    {
        [XmlElement("Car")]
        public Car[] Cars { get; set; }
    }
}
