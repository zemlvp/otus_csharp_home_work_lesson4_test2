﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace test1
{
    class Program
    {
        // тестовый пример по десериализации через ExtendedXmlSerializer
        static void Main()
        {
            string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = Path.Combine(executableLocation, "test.xml");

            var fs = new FileStream(path, FileMode.Open);
            {
                IExtendedXmlSerializer extendedXmlSerializer = new ConfigurationContainer()
                     //.UseAutoFormatting()
                     //.UseOptimizedNamespaces()
                     //.EnableImplicitTyping(typeof(CarCollection))
                     .Create();

                XmlSerializer standartSerializer = new XmlSerializer(typeof(CarCollection));

                // стандартный сериализатор работает, расширенный нет
                var cars1 = standartSerializer.Deserialize(fs);
                var cars2 = extendedXmlSerializer.Deserialize<CarCollection>(fs);
            }
        }
    }
}
